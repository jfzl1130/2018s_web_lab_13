package ictgradschool.web.lab13.ex1;

import com.sun.org.apache.bcel.internal.generic.Select;
import web_lab_13_demo.src.ictgradschool.web.jdbc.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            //Anything that requires the use of the connection should be in here...

            int count = 0;
            while (count == 0) {

                String partialTitle = Keyboard.readInput("Enter the partial title of the articles: ");


                try (Statement articleTitle = conn.createStatement()) {
                    try (ResultSet resultSet = articleTitle.executeQuery("SELECT title FROM lab13_articles WHERE title LIKE '%" + partialTitle + "%';")) {
                        while (resultSet.next()) {
                            System.out.println(resultSet.getString(1));
                            count++;
                        }
                    }






        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}catch(SQLException e) {
        e.printStackTrace();
    }


}

        }
