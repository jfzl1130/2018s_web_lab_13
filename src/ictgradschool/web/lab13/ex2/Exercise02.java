package ictgradschool.web.lab13.ex2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Exercise02 {

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning.*/
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)){
            System.out.println("Welcome to the Film World!");
            choices(conn, dbProps);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void choices (Connection conn, Properties dbProps){
        System.out.println("Please select an option of the following:");
        System.out.println("1: Information by Actor");
        System.out.println("2: Information by Movie");
        System.out.println("3: Information by Genre");
        System.out.println("4: Exit");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            int userChoice = Integer.parseInt(br.readLine());
            switch (userChoice){
                case 1: informationByActor(conn, dbProps);
                    break;
                case 2: informationByMovie(conn, dbProps);
                    break;
                case 3: informationByGenre(conn, dbProps);
                    break;
                case 4: exitTime();
                    break;
                default:
                    System.out.println("Invalid Choice, please try again");
                    choices(conn, dbProps);
                    break;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void informationByActor (Connection conn, Properties dbProps) throws IOException {
        System.out.println("Please enter the name of the actor you want to know about, or press enter back to the main menu.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String actorName = br.readLine();
        if(actorName.isEmpty()){
            System.out.println("The name does not exist \n");
            choices(conn, dbProps);
        }else{
            try (PreparedStatement stmt = conn.prepareStatement("SELECT PF.film_title, PR.role_name FROM pfilms_actor AS PA, pfilms_participates_in AS PPI, pfilms_film AS PF, pfilms_role AS PR WHERE (PA.actor_fname LIKE ? OR PA.actor_lname LIKE ?) AND PA.actor_id = PPI.actor_id AND PF.film_id = PPI.film_id AND PR.role_id = PPI.role_id")){
                stmt.setString(1,actorName);
                stmt.setString(2,actorName);

                try (ResultSet r = stmt.executeQuery()){
                    List<String> filmName = new ArrayList<>();
                    List<String> roleName =new ArrayList<>();
                    while (r.next()){
                        filmName.add(r.getString(1));
                        roleName.add(r.getString(2));
                    }
                    System.out.println(actorName+" is listed as being involved in the following films: ");
                    System.out.println();
                    for (int i = 0; i <filmName.size();i++) {
                        System.out.println(filmName.get(i)+"("+roleName.get(i)+")\n");
                    }

                }
                choices(conn,dbProps);


            }
            catch (SQLException e){
                System.out.println("error");
            }
        }
    }
    public static void informationByMovie (Connection conn, Properties dbProps){
    }
    public static void informationByGenre (Connection conn, Properties dbProps){}
    public static void exitTime (){
        System.out.println(" Thank you ! Good Bye");
    }





}
