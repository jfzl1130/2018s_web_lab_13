package web_lab_13_demo.src.ictgradschool.web.jdbc;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class ArticleBrowser {

    private static final int LIST_ALL = 1, GET_SINGLE = 2, ADD_ARTICLE = 3, DELETE_ARTICLE = 4, EXIT = 5;

    public static void main(String[] args) {
        new ArticleBrowser().start();
    }

    private void displayMenu() {

        System.out.println("Menu:");
        System.out.println("1) List all articles");
        System.out.println("2) Get single article");
        System.out.println("3) Add article");
        System.out.println("4) Delete article");
        System.out.println("5) Exit");

    }

    private void start() {

        // This code makes sure that the MySQL driver is loaded.
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Get the connection properties
        Properties props = loadProperties();

        // Creates a connection to our database.
        // try-with-resources is used so the connection is properly closed when we're done.
        try (Connection conn = DriverManager.getConnection(props.getProperty("url"), props)) {

            // This DAO contains all the SQL code and is used to interact with the database.
            ArticleDAO dao = new ArticleDAO(conn);

            while (true) {

                displayMenu();

                int command = Keyboard.readInt("Enter your choice: ");

                switch (command) {

                    case LIST_ALL:
                        displayAllArticles(dao);
                        break;

                    case GET_SINGLE:
                        displaySingleArticle(dao);
                        break;

                    case ADD_ARTICLE:
                        addArticle(dao);
                        break;

                    case DELETE_ARTICLE:
                        deleteArticle(dao);
                        break;

                    case EXIT:
                        System.out.println("Thanks for using the system!");
                        return;

                    default:
                        System.out.println("Unknown command, please try again.");
                        break;

                }

                System.out.println();

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private void displayAllArticles(ArticleDAO dao) throws SQLException {

        // All this code (and the code in the following methods) is really simple
        // because we've encapsulated all our DB access code in a separate class
        // (the ArticleDAO class).

        List<Article> articles = dao.getAllArticles();

        for (Article a : articles) {
            System.out.println(a);
            System.out.println();
        }

    }

    private void displaySingleArticle(ArticleDAO dao) throws SQLException {

        int id = Keyboard.readInt("Please enter the id: ");

        Article article = dao.getArticleById(id);

        if (article == null) {
            System.out.println("Not found!");
        }
        else {
            System.out.println(article);
        }

    }

    private void addArticle(ArticleDAO dao) throws SQLException {

        String title = Keyboard.readInput("Enter the title: ");
        String content = Keyboard.readInput("Enter the content: ");
        Article article = new Article(title, content);

        dao.addArticle(article);

    }

    private void deleteArticle(ArticleDAO dao) throws SQLException {

        int id = Keyboard.readInt("Enter the article id: ");

        dao.deleteArticle(id);
    }

    private Properties loadProperties() {

        // Open the jdbc.properties file
        // (again, try-with-resources so the file is closed properly later)
        try (FileInputStream fIn = new FileInputStream("jdbc.properties")) {

            // Create the properties object and load the file contents into it.
            Properties props = new Properties();
            props.load(fIn);
            return props;

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
